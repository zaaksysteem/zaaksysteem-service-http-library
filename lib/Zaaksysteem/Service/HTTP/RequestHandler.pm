package Zaaksysteem::Service::HTTP::RequestHandler;

use Moose::Role;

requires qw[
    dispatch
    log
];

=head1 NAME

Zaaksysteem::Service::HTTP::RequestHandler - Request handler role

=head1 SYNOPSIS

    package My::Fancy::uService::RequestHandlers::Foo;

    with 'Zaaksysteem::Service::HTTP::RequestHandler';

    ...

    sub dispatch {
        return { foo => 'bar' };
    }

=cut

use BTTW::Tools;

use Plack::Request;
use JSON::XS;
use Syzygy::Object::Model;
use UUID::Tiny qw[create_uuid_as_string UUID_V4];

=head1 INTERFACE REQUIREMENTS

=head2 dispatch

Consumers of this role must implement a C<dispatch> method, which must return
a hashref as response data.

Dispatch methods may throw an exception, which will be processed via
L</process_error>.

=cut

sig dispatch => '=> Maybe[Syzygy::Object]';

=head2 log

Expected to be a reference to an object which can be used to log
application-level messages.

In all but the most exotic case this should be implemented using the
MooseX::Log::Log4perl role.

=cut

sig log => '=> Object';

=head1 ATTRIBUTES

=head2 service

Reference to the L<Zaaksysteem::Service::HTTP> instance for which this handler
was instantiated.

=cut

has service => (
    is => 'ro',
    isa => 'Zaaksysteem::Service::HTTP',
    required => 1
);

=head2 json

A reference to a L<JSON::XS>-compatible object for (de)serializing request
and response data.

=cut

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub { JSON::XS->new }
);

=head1 METHODS

=head2 to_app

Implements the L<Plack> C<to_app> API.

This method calls L</dispatch> of the consuming class, and manages error
catching during the dispatch.

=cut

sub to_app {
    my $self = shift;

    my $service = $self->service;

    return sub {
        my $env = shift;
        my $request = Plack::Request->new($env);

        # Create random request ID
        my $request_id = create_uuid_as_string(UUID_V4);

        $request->request_body_parser->register(
            'application/json' => 'HTTP::Entity::Parser::JSON'
        );

        # Reset debug contexts
        $service->reset_log_context;

        $service->set_log_context(id => $request_id);
        $service->set_log_context(request_uri => sprintf('/%s', $self->name));
        $service->set_log_context(remote_ip => $request->address);
        $service->set_log_context(method => $request->method);

        my $http_status_code = 200;

        my $result = try {
            return $self->dispatch($request);
        } catch {
            # Break domain for a bit, exceptions may contain an explicitly
            # desired http status code. Attempt lyfting it up to our level,
            # otherwise default to internal-server-error.
            $http_status_code = eval { $_->http_code } || 500;

            return $self->_process_error($_);
        };

        $service->set_log_context(status => $http_status_code);

        my $response = $request->new_response($http_status_code);

        if (defined $result) {
            $response->header('Content-Type' => 'application/json');
            $response->body($self->json->encode($result->as_graph_hash));
        };

        return $response->finalize;
    };
}

=head1 PRIVATE METHODS

=head2 _process_error

Generic error handler used by L</to_app> in case of a caught exception.

Returns a L<Syzygy::Object> instances with the
L<Syzygy::Object::Types::Exception> type, instantiated with the message and an
exception type. The transformer can handle L<BTTW::Exception>-style
exceptions completely, and other errors are stringified and typed C<error>.

=cut

sub _process_error {
    my $self = shift;
    my $error = shift;

    my $error_type;
    my $error_message;

    if (blessed $error && $error->isa('BTTW::Exception::Base')) {
        $error_type       = $error->type;
        $error_message    = $error->message;
    }

    $error_message //= "$error";
    $error_type    //= "error";

    $self->log->warn(sprintf(
        'Caught %s exception: %s',
        $error_type || 'unknown',
        $error_message,
    ));

    return Syzygy::Object::Model->new_object(exception => {
        type => $error_type,
        message => $error_message,
        timestamp => DateTime->now
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
