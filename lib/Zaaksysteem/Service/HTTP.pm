package Zaaksysteem::Service::HTTP;

use Moose;
use version;

our $VERSION = version->declare('v0.0.1');

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Service - Microservice factory library

=head1 ABSTRACT

Service module C<uService.pm>

    package My::Fancy::uService;

    extends 'Zaaksysteem::Service::HTTP';

    ....

PSGI bootstrap C<my_app.psgi>

    #!/usr/bin/perl -w

    use strict

    use My::Fancy::uService;

    My::Fancy::uService->new->to_app;

=cut

use BTTW::Tools;

use Log::Log4perl qw[:levels];
use Log::Log4perl::MDC qw[];
use Plack::Builder;
use Config::General;
use Module::Pluggable::Object;
use List::Util qw[all];

=head1 ATTRIBUTES

=head2 log4perl_config_file

Path of the L<Log::Log4perl> configuration file to be used on instantiation of
the service.

=cut

has log4perl_config_file => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_log4perl_config_file'
);

=head2 service_config_file

Path of the service configuration file to be used on instantiation of the
service.

=cut

has service_config_file => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_service_config_file'
);

=head2 service_config

Parsed content of the service configuration file.

=cut

has service_config => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    builder => 'build_service_config'
);

=head1 METHODS

=cut

sub BUILD {
    my $self = shift;

    if ($self->has_log4perl_config_file && -f $self->log4perl_config_file) {
        Log::Log4perl->init($self->log4perl_config_file);
    } else {
        Log::Log4perl->easy_init($TRACE);
    }

    Log::Log4perl->get_logger->info(sprintf(
        'Initializing Zaaksysteem HTTP Service Library %s',
        $VERSION
    ));
}

=head2 to_app

Returns a L<Plack> compatible application representing the service.

=cut

sub to_app {
    my $self = shift;

    my $builder = Plack::Builder->new;

    for my $handler ($self->find_request_handlers) {
        my $mountpoint = sprintf('/%s', $handler->name);

        $builder->mount($mountpoint, $handler->new(service => $self)->to_app);
    }

    return $builder->to_app;
}

=head2 find_request_handlers

Searches the filesystem for modules that implement the
L<Zaaksysteem::Service::HTTP::RequestHandler> role.

By default the C<RequestHandlers> namespace from the instance package name
will be used, but this can be overridden in the service configuration file.

    # in service.conf
    <Zaaksysteem::Service::HTTP>
        request_handler_namespace = My::Fancy::uService::Handlers
    </Zaaksysteem::Service::HTTP>

=cut

sub find_request_handlers {
    my $self = shift;

    my $search_ns = eval {
        $self->config->{ 'Zaaksysteem::Service::HTTP' }{ request_handler_namespace }
    } // sprintf('%s::RequestHandlers', $self->meta->name);

    my $finder = Module::Pluggable::Object->new(
        search_path => $search_ns,
        require => 1
    );

    my @handlers = $finder->plugins;

    my $ok = all {
        try { $_->does('Zaaksysteem::Service::HTTP::RequestHandler') }
        catch { $self->log->error($_); return; }
    } @handlers;

    unless ($ok) {
        throw('service/request_handlers/load_unsuccesful', sprintf(
            'Could not load some request handlers in "%s" namespace',
            $search_ns
        ));
    }

    return @handlers;
}

=head2 build_service_config

Builder for the L</service_config> attribute.

Attempts to load the configuration file at L</service_config_file>, and
returns and L</default_service_config> otherwise.

=cut

sub build_service_config {
    my $self = shift;

    unless ($self->has_service_config_file) {
        $self->log->info(
            'No service configuration file specified, using default configuration'
        );

        return $self->default_service_config;
    }

    my $file = $self->service_config_file;

    unless (-f $file && -r $file) {
        $self->log->info(sprintf(
            'Service configuration file "%s" not found, using default configuration',
            $file
        ));

        return $self->default_service_config;
    }

    my $config = Config::General->new(-ConfigFile => $file);

    return { $config->getall };
}

=head2 set_log_context

Wrapper around L<Log::Log4perl::MDC/put> as convenience for request handlers.

=cut

sub set_log_context {
    my $self = shift;

    Log::Log4perl::MDC->put(@_);

    return;
}

=head2 reset_log_context

Wrapper around L<Log::Log4perl::MDC/remove> as convenience for request
handlers.

=cut

sub reset_log_context {
    my $self = shift;

    Log::Log4perl::MDC->remove;

    return;
}

=head2 default_service_config

Default service configuration. Useful for services with static configuration.

    # in My::Fancy::uService

    override default_service_config => sub {
        return { ... };
    };

Returns an empty hashref unless overridden.

=cut

sub default_service_config {
    return {};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
