FROM registry.gitlab.com/zaaksysteem/platform:58a06fbd

WORKDIR /tmp/zaaksysteem-service-http-src

COPY Makefile.PL .

RUN cpanm --installdeps Log::Log4perl::Appender::Fluent && cpanm --notest Log::Log4perl::Appender::Fluent

RUN cpanm --installdeps . \
 && rm -rf ~/.cpanm \
 || (cat ~/.cpanm/work/*/build.log && false)

COPY . .

RUN perl Makefile.PL && make install
